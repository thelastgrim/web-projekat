package database;

import java.io.File;
import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import beans.Aktivnost;
import beans.Disk;
import beans.Kategorija;
import beans.Korisnik;
import beans.Organizacija;
import beans.TipDiska;
import beans.VM;

public class Database {
	private static final double CENA_JEZGRA = 0.0347222;
	private static final double CENA_GB_RAMA = 0.0208333;
	private static final double CENA_GPU_JEZGRA = 0.0013888;
	private static final double CENA_HDD_GB = 0.0001388;
	private static final double CENA_SDD_GB = 0.0004166;
	
	public static ArrayList<Organizacija> organizacije = new ArrayList<Organizacija>();
	public static ArrayList<Korisnik> korisnici = new ArrayList<Korisnik>();
	public static ArrayList<VM> VMs = new ArrayList<VM>();
	public static ArrayList<Disk> diskovi = new ArrayList<Disk>();
	private static FileWriter file;
	public static ArrayList<Kategorija> kategorije= new ArrayList<Kategorija>();
	
	
	public static void saveOrganizacije() {
		
		String json = new Gson().toJson(organizacije);
		
		try {
			file = new FileWriter(new File("src/database/organizacije.json"));
			file.write(json);
			file.close();
	
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public static void saveKorisnici() {
		
		String json = new Gson().toJson(korisnici);
		
		try {
			file = new FileWriter(new File("src/database/korisnici.json"));
			file.write(json);
			file.close();
	
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public static void saveVMs() {
		
		String json = new Gson().toJson(VMs);
		
		try {
			file = new FileWriter(new File("src/database/VMs.json"));
			file.write(json);
			file.close();
	
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		
	}
	
	public static void saveDiskovi() {
		String json = new Gson().toJson(diskovi);
		
		try {
			file = new FileWriter(new File("src/database/diskovi.json"));
			file.write(json);
			file.close();
	
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	public static void saveKategorije() {
		String json = new Gson().toJson(kategorije);
		
		try {
			file = new FileWriter(new File("src/database/kategorije.json"));
			file.write(json);
			file.close();
	
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}
	
	
	public static ArrayList<VM> getVMsByOrganizacijaName(String organizacijaIme){
		
		ArrayList<VM> vms = new ArrayList<VM>();
		
		for (VM vm : VMs) {
			if(vm.getOrganizacijaIme().equals(organizacijaIme)) {
				vms.add(vm);
			}
		}
		
		return vms;
		
	};
	
	
	
	/*
	public static Set<String> getAllDiskoviNames() {
		Set<String> _diskovi = new HashSet<String>();
		
		for (Disk d : diskovi) {
			_diskovi.add(d.getIme());
		}
		
		return _diskovi;
	}
	*/
	
	
	
	public static VM getVMByName(String name) {
		
		for (VM vm : VMs) {
			if(vm.getIme().equals(name)) {
				return vm;
			}
		}
		
		return null;
	}
	
	
	public static Organizacija getOrganzacijaByName(String name) {
		
		for (Organizacija org : organizacije) {
			if(org.getIme().equals(name)) {
				return org;
			}
		}
		
		return null;
	}
	
	private static Korisnik getKorisnikByEmail(String email) {
		
		for (Korisnik k : korisnici) {
			if(k.getEmail().equals(email)) {
				return k;
			}
		}
		
		return null;
	}
	
	
	public static boolean addKorisnik(Korisnik k) {
		
		korisnici.add(k);
		
		for (Organizacija org : organizacije) {
			if(org.getIme().equals(k.getOrganizacijaIme())) {
				org.getKorisnici().add(k);
				
				break;
			}
		}
		
		saveKorisnici();
		saveOrganizacije();
		
		return true;
		
	}
	
	public static boolean deleteKorisnikByEmail(String email) {
		
		boolean userPassed = false;
		boolean orgsPassed = false;
		
		for (Organizacija org : organizacije) {
			if(org.getIme().equals(getKorisnikByEmail(email).getOrganizacijaIme())) {
				for (Iterator<Korisnik> i = org.getKorisnici().iterator(); i.hasNext();) {
				    Korisnik k = i.next();
				    if (k.getEmail().equals(email)) {
				    	i.remove();
				    	orgsPassed = true;
				    	break;
				    }
				}
				break;
			}
		}

		for (Iterator<Korisnik> i = korisnici.iterator(); i.hasNext();) {
		    Korisnik k = i.next();
		    if (k.getEmail().equals(email)) {
		    	i.remove();
		    	userPassed = true;
		    	break;
		    }
		}
		
		
		
		if (userPassed && orgsPassed) {
			saveKorisnici();
			saveOrganizacije();
			return true;
		}
		else {
			return false;
		}
	}
	
	public static Korisnik updateProfile(Korisnik k, Korisnik old) {
	
		Korisnik newK= null;
		String oldEmail = old.getEmail();
		
		for (Korisnik korisnik : korisnici) {
			if (korisnik.getEmail().equals(old.getEmail())) {
				korisnik.setIme(k.getIme());
				
				korisnik.setPrezime(k.getPrezime());
				
				if(!k.getLozinka().equals("")) {
					korisnik.setLozinka(k.getLozinka());
					System.out.println("prom");
					
				}
					
				korisnik.setEmail(k.getEmail());
				newK = korisnik;
				break;
			}
		}
		
		for (Organizacija org : organizacije) {
			if(org.getIme().equals(old.getOrganizacijaIme())) {
				for (Korisnik korisnik : org.getKorisnici()) {
					if (korisnik.getEmail().equals(oldEmail)) {
						korisnik.setIme(k.getIme());
						korisnik.setPrezime(k.getPrezime());
						if(!k.getLozinka().equals("")) {
							korisnik.setLozinka(k.getLozinka());
							System.out.println("prom");
						}
							
						korisnik.setEmail(k.getEmail());
						break;
					}
				}
				break;
			}
		}
		
		saveKorisnici();
		saveOrganizacije();
		
		return newK;
		
	}

	private static String getOrganizacijaImeByKorisnikEmail(String email) {
		
		for (Korisnik korisnik : korisnici) {
			if(korisnik.getEmail().equals(email)) {
				return korisnik.getOrganizacijaIme();
			}
		}
		
		return null;
	}
	
	public static Kategorija getKategorijaByName(String name) {
		
		for (Kategorija kat : kategorije) {
			if(kat.getIme().equals(name)) {
				return kat;
			}
		}
		
		return null;
	}
	
	public static Disk getDiskByName(String name) {
		
		for (Disk d : diskovi) {
			if(d.getIme().equals(name)) {
				return d;
			}
		}
		
		return null;
	}
	
	public static void updateVM(JsonObject job) {
		
		String oldName = job.get("oldName").getAsString();
		String newName = job.get("newName").getAsString();
		String kategorija = job.get("kategorija").getAsString();
		JsonArray diskovi = job.get("diskovi").getAsJsonArray();
		String organizacija = job.get("org").getAsString();
		for (Organizacija org : organizacije) {
			
			if(org.getIme().equals(organizacija)) {
				
				for (Iterator<String> i = org.getResursi().iterator(); i.hasNext();) {
				    String resurs = i.next();
				    if (resurs.equals(oldName)) {
						i.remove();
						break;
					}
				    
				    
				}
				
				org.getResursi().add(newName);
				
				
				break;
			}
			
			
		}
		
		ArrayList<Disk> stariDiskovi = null;
		
		for (VM vm : VMs) {
			if (vm.getIme().equals(oldName)) {
				
				stariDiskovi = new ArrayList<Disk>(vm.getDiskovi());
		
				vm.setIme(newName);
				vm.setKategorija(getKategorijaByName(kategorija));
				ArrayList<Disk> _diskovi = new ArrayList<Disk>();
				for (JsonElement disk : diskovi) {
					Disk newD = getDiskByName(disk.getAsString().split("\\[")[0].trim());
					newD.setVirtuelnaMasinaIme(newName);
					_diskovi.add(newD);
					stariDiskovi.removeIf(d -> d.getIme().equals(newD.getIme()));
				}
				
				vm.setDiskovi(_diskovi);
				
				break;		
			}
		}
		
		for (Disk disk : stariDiskovi) {
			oslobodiDisk(disk);
		}
	
		
		saveDiskovi();
		saveVMs();
		saveOrganizacije();
	}
	
	

	public static boolean updateKorisnik(Korisnik k) {
	
		
		for (Korisnik korisnik : korisnici) {
			if (korisnik.getEmail().equals(k.getEmail())) {
				korisnik.setIme(k.getIme());
				korisnik.setPrezime(k.getPrezime());
				if(!k.getLozinka().equals("")) {
					korisnik.setLozinka(k.getLozinka());
				}
				break;
			}
		}
		
		for (Organizacija org : organizacije) {
			if(org.getIme().equals(getOrganizacijaImeByKorisnikEmail(k.getEmail()))) {
				for (Korisnik korisnik : org.getKorisnici()) {
					if (korisnik.getEmail().equals(k.getEmail())) {
						korisnik.setIme(k.getIme());
						korisnik.setPrezime(k.getPrezime());
						break;
					}
				}
				break;
			}
		}
		
		saveKorisnici();
		saveOrganizacije();
		
		return true;
		
	}

	public static Aktivnost turnOn(String name) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
	
		Date now = null;;
		try {
			now = sdf.parse(sdf.format(new Date()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Aktivnost a = new Aktivnost(now, null);
		for (VM vm : VMs) {
			if(vm.getIme().equals(name)) {
				vm.getAktivnosti().add(a);
				break;
			}
		}
		
		saveVMs();
		
		return a;
	}
		
	public static Aktivnost turnOff(String name) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date now = null;
		
		try {
			now = sdf.parse(sdf.format(new Date()));
		} catch (Exception e) {
			// TODO: handle exception
		}
		Aktivnost a = null;
		for (VM vm : VMs) {
			if(vm.getIme().equals(name)) {
				vm.getAktivnosti().get(vm.getAktivnosti().size()-1).setKrajRada(now);
				a = vm.getAktivnosti().get(vm.getAktivnosti().size()-1);
				break;
			}
		}
		
		saveVMs();
		
		return a;
	}
	
	public static JsonElement getReport(String _d1, String _d2, String _orgName) throws ParseException {
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		
		
		
		Date from = sdf.parse(_d1);
		Date to = sdf.parse(_d2);
		
		JsonArray jar = new JsonArray();
		for (VM vm : VMs) {
			JsonObject job = new JsonObject();
			if(vm.getOrganizacijaIme().equals(_orgName)) {
				int hours = 0;
				for (Aktivnost aktivnost : vm.getAktivnosti()) {
					long milliseconds = 0;
					
					
					
					
					try {
						if(aktivnost.getPocetakRada().after(from) && aktivnost.getKrajRada().before(to)) {
							milliseconds = aktivnost.getKrajRada().getTime() - aktivnost.getPocetakRada().getTime();
							
							
						}
						else if(aktivnost.getPocetakRada().after(from) && aktivnost.getKrajRada().after(to)) {
							milliseconds = to.getTime()-aktivnost.getPocetakRada().getTime();
							
						}
						else if(aktivnost.getPocetakRada().before(from) && aktivnost.getKrajRada().before(to)) {
							milliseconds = aktivnost.getKrajRada().getTime()-from.getTime();
							
							
						}else if(aktivnost.getPocetakRada().before(from) && aktivnost.getKrajRada().after(to)) {
							milliseconds = to.getTime()-from.getTime();
							
						}else {
							System.out.println("ne bi trebalo da udje");
						}
						if (milliseconds > 0 ) {
							hours   = hours + (int) ((milliseconds / (1000*60*60)));
						}
						
						
					} catch (NullPointerException e) {
						System.out.println("Ne postoji datum gasenja");
					}	
				}
				long milliseconds_disk = 0;
				Date startovanje_diska = vm.getAktivnosti().get(0).getPocetakRada();
				
				if(from.before(startovanje_diska)) {
					milliseconds_disk = to.getTime()-startovanje_diska.getTime();
				}else {
					milliseconds_disk = to.getTime() - from.getTime();
				}
				
				
				double hours_disk   = (int) ((milliseconds_disk / (1000*60*60)));
				for (Disk disk : vm.getDiskovi()) {
					job = new JsonObject();
					double profit_diska = 0;
					if (disk.getTip()==TipDiska.SSD && hours_disk > 0) {
						profit_diska = disk.getKapacitet()*hours_disk*CENA_SDD_GB;
					}else if (disk.getTip()==TipDiska.HDD && hours_disk > 0) {
						profit_diska = disk.getKapacitet()*hours_disk*CENA_HDD_GB;
					}
					
					if(profit_diska>0) {
						job.add("ime", new JsonParser().parse(disk.getIme()));
						job.add("profit", new JsonParser().parse(String.valueOf(profit_diska)));
						jar.add(job);
					}
					
				}
				
				
				
				job = new JsonObject();
				double profit = (vm.getKategorija().getBrojJezgara()*CENA_JEZGRA+
								vm.getKategorija().getGPUJezgra()*CENA_GPU_JEZGRA+
								vm.getKategorija().getRAM()*CENA_GB_RAMA)*hours;
				if(profit > 0) {
					job.add("ime", new JsonParser().parse(vm.getIme()));
					job.add("profit", new JsonParser().parse(String.valueOf(profit)));
					jar.add(job);
				}
				
			}
			
		}
		
		
		
		
		return jar;
	}
	

	//mosjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjjj
	
	
	private static void oslobodiDisk(Disk disk) {
		
		for (Disk d : diskovi) {
			if(d.getIme().equals(disk.getIme())) {
				d.setVirtuelnaMasinaIme("");
				break;
			}
		}
		
	}
	
	public static Set<String> getAllOrganizacijeNames() {
		Set<String> orgs = new HashSet<String>();
		
		for (Organizacija o : organizacije) {
			orgs.add(o.getIme());
		}
		
		return orgs;
	}
	
	public static ArrayList<Disk> JsArrToObjDisk(JsonArray jar){
		
		ArrayList<Disk> disks = new ArrayList<Disk>();
		
		for (JsonElement d : jar) {
			
			String dName = d.getAsString().split("\\[")[0].trim();
			
			disks.add(getDiskByName(dName));
		}
		
		return disks;
		
	}

	public static ArrayList<String> getAllKategorijeFormated() {
		ArrayList<String> katz = new ArrayList<String>();
		
		for (Kategorija kat : kategorije) {
			String formated = kat.getIme()+"  [brojJezgara = "+kat.getBrojJezgara()+","+
					   " RAM(GB) = "+kat.getRAM()+","+
					   " GPU(Jezgra) = "+kat.GPUJezgra+"]";
			katz.add(formated);
		}

		
		return katz;
	}

	public static ArrayList<String> getAllAvailableDiskoviByOrganizacijaName(String org) {
		ArrayList<String> disks = new ArrayList<String>();
		
		for (Disk d : diskovi) {
			if(d.getOrganizacijaIme().equals(org) && d.getVirtuelnaMasinaIme().equals("")) {
				String dFormated = d.getIme() + " ["+d.getKapacitet()+"GB]";
				disks.add(dFormated);
			}
		}
		return disks;
	}

	public static void addVM(JsonObject job) {
		
		String ime = job.get("ime").getAsString();
		String org = job.get("org").getAsString();
		String kategorija = job.get("kategorija").getAsString();
		JsonArray _diskovi = job.get("diskovi").getAsJsonArray();
		
		ArrayList<Disk> disks = JsArrToObjDisk(_diskovi);
		
		for (Disk d1 : diskovi) {
			for (Disk d2 : disks) {
				if(d1.getIme().equals(d2.getIme())) {
					d1.setVirtuelnaMasinaIme(ime);
					d2.setVirtuelnaMasinaIme(ime);
				}
			}
		}
		ArrayList<Aktivnost> prazna = new ArrayList<Aktivnost>();
		
		VMs.add(new VM(ime, org, getKategorijaByName(kategorija), disks, prazna));
		
		for (Organizacija org2 : organizacije) {
			if(org2.getIme().equals(org)) {
				org2.getResursi().add(ime); 
				
				break;
			}
		}
		
		
		
		saveOrganizacije();
		saveVMs();
		saveDiskovi();
		
		
	}

	
	public static boolean deleteVMByName(String vmName) {
		
		boolean first = false;
		boolean second = false;
		
		for (Disk d1 : diskovi) {
			for (Disk d2 : getVMByName(vmName).getDiskovi()) {
				if(d1.getIme().equals(d2.getIme())) {
					d1.setVirtuelnaMasinaIme("");
				}
			}
		}
		
		for (Iterator<VM> i = VMs.iterator(); i.hasNext();) {
			if(i.next().getIme().equals(vmName)) {
				i.remove();
				first = true;
				break;
			}
		}
		
		
		for (Organizacija org : organizacije) {
			
			for (String str : org.getResursi()) {
				if(str.equals(vmName)) {
					org.getResursi().remove(org.getResursi().indexOf(vmName));
					second = true;
					break;
				}
			}
		}
		saveDiskovi();
		saveOrganizacije();
		saveVMs();
		
		if(second && first) {
			return true;
		}else {
			return false;
		}
		
	
		
		
	}
	
		
	public static ArrayList<String> getAllVMsNamesByOrgName(String org){
		ArrayList<String> vms = new ArrayList<String>();
		
		for (VM vm : VMs) {
			if(vm.getOrganizacijaIme().equals(org)) {
				vms.add(vm.getIme());
			}
			
		}
		
		return vms;
	}

	public static boolean updateDisk(JsonObject job) {

		boolean first = false;
		boolean second = false;
		boolean third = false;
		
		String oldName = job.get("oldName").getAsString();
		String newName = job.get("newName").getAsString();
		int kapacitet = job.get("kapacitet").getAsInt();
		String newVM = job.get("vm").getAsString();
		String tip = job.get("tip").getAsString();
		
		String oldVM = null;
		
		Disk d = new Disk(getDiskByName(oldName));
		Disk novi = new Disk();
		
		for (Disk dd : diskovi) {
			if(dd.getIme().equals(oldName)) {
				dd.setIme(newName);
				dd.setKapacitet(kapacitet);
				dd.setTip(tip.equals("HDD")?TipDiska.HDD:TipDiska.SSD);
				oldVM = dd.getVirtuelnaMasinaIme();
				dd.setVirtuelnaMasinaIme(newVM);
				novi = dd;
				
				break;
				
			}
		}
		
		
		
		first = true;
		saveDiskovi();
		
		
		if(!newVM.equals("")) {
			if(!d.getVirtuelnaMasinaIme().equals(newVM)) {
				/*
				 * promenjena virtuelna masina
				 * treba raskaciti
				 */
				
				for (VM vm : VMs) {
					if(vm.getIme().equals(d.getVirtuelnaMasinaIme())) {
						
						vm.getDiskovi().removeIf(d2 -> d2.getIme().equals(d.getIme()));
						break;
					}
				}
				//otkaceni sa masine
				
				/*
				 * treba zakaciti na novu
				 */
				
				for (VM vm : VMs) {
					if(vm.getIme().equals(newVM)) {
						vm.getDiskovi().add(novi);
						break;
					}
				}
				
			}else {
				/*
				 * na istoj masini promini disk
				 */
				for (VM vm : VMs) {
					if(vm.getIme().equals(newVM)) {
						vm.getDiskovi().removeIf(ds -> ds.getIme().equals(oldName));
						vm.getDiskovi().add(novi);
						break;
					}
				}
				
			}
		}else {
			// izabrano "BEZ VM"
			for (VM vm : VMs) {
				if(vm.getIme().equals(oldVM)) {
					
					vm.getDiskovi().removeIf(d2 -> d2.getIme().equals(d.getIme()));
					break;
				}
			}
			
		}
		
		second = true;
		saveVMs();
	
		for (Organizacija org : organizacije) {
			if(org.getIme().equals(novi.getOrganizacijaIme())) {
				if(!newName.equals(oldName)) {
					org.getResursi().remove(org.getResursi().indexOf(oldName));
					org.getResursi().add(newName);
					saveOrganizacije();
					break;
				}
			}
		}
		third = true;
		
		if(second && first && third) {
			return true;
		}else {
			return false;
		}
		
	}

	public static boolean addDisk(JsonObject job) {
		
		boolean second = false;
		boolean third = false;
		String org = job.get("org").getAsString();
		String ime = job.get("ime").getAsString();
		int kapacitet = job.get("kapacitet").getAsInt();
		String tip = job.get("tip").getAsString();
		String vm = job.get("vm").getAsString();
		
		Disk newDisk = new Disk(ime, org, vm, tip.equals("HDD")?TipDiska.HDD:TipDiska.SSD, kapacitet);
		
		diskovi.add(newDisk);
		
		saveDiskovi();
		
		if(!vm.equals("")) {
			for (VM vm2 : VMs) {
				if(vm2.getIme().equals(vm)) {
					vm2.getDiskovi().add(newDisk);
					second = true;
					break;
				}
			}
		}else {
			second = true;
		}
		
		saveVMs();
		
		
		for (Organizacija org2 : organizacije) {
			if(org2.getIme().equals(org)) {
				org2.getResursi().add(ime); 
				third = true;
				break;  //kad ima isti disk i vm???
			}
		}
		
		
		
		saveDiskovi();
		
		
		
		return second&&third;
	}

	public static boolean deleteDiskByName(String dName) {
		
		Disk newD = getDiskByName(dName);
		
		diskovi.removeIf(d->d.getIme().equals(dName));
		
		for (VM vm : VMs) {
			if(vm.getIme().equals(newD.getVirtuelnaMasinaIme())) {
				vm.getDiskovi().removeIf(d -> d.getIme().equals(dName));
				break;
			}
		}
		for (Organizacija org : organizacije) {
			if(org.getIme().equals(newD.getOrganizacijaIme())) {
				org.getResursi().removeIf(r -> r.equals(dName)); //obrisace i vm ako se zove isto kao disk
				break;
			}
		}
		
		saveDiskovi();
		saveOrganizacije();
		saveVMs();
		
		
		
		return true;
	}

	public static boolean addKatetorija(Kategorija k) {

		kategorije.add(k);
		
		saveKategorije();
		
		return true;
	}

	public static boolean updateKategorija(String old_name, Kategorija k) {
		
		boolean changed = false;
		for (Kategorija ka : kategorije) {
			if(ka.getIme().equals(old_name)) {
				kategorije.set(kategorije.indexOf(ka), k);
				changed = true;
				break;
			}
		}
		
		if (changed) {
			saveKategorije();
			
			for (VM vm : VMs) {
				if(vm.getKategorija().getIme().equals(old_name)) {
					vm.setKategorija(k);
				}
			}
			
			return changed;
		}
		
		return changed;
	}

	public static boolean deleteKategorijaByName(String old_name) {
		
		boolean delete = false;
		
		for (VM vm : VMs) {
			if(vm.getKategorija().getIme().equals(old_name)) {
				return delete;
			}
		}
		
		
		for (Iterator<Kategorija> i = kategorije.iterator(); i.hasNext();) {
		    Kategorija k = i.next();
		    if (k.getIme().equals(old_name)) {
		    	i.remove();
		    	delete = true;
		    	break;
		    }
		}
		
		if (delete) {
			saveKategorije();
		}
		
		return delete;
	}

	
	
}
