package rest;

import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;
import static spark.Spark.staticFiles;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import javax.servlet.MultipartConfigElement;
import javax.servlet.http.Part;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import beans.Disk;
import beans.Kategorija;
import beans.Korisnik;
import beans.Organizacija;
import beans.Uloga;
import beans.VM;
import database.Database;
import functionalities.Sistem;
import spark.Session;

public class Main {
	
	private static Gson g = new Gson();

	public static void main(String[] args) throws IOException {
		System.out.println("Working Directory = " + System.getProperty("user.dir"));
		Sistem.ucitavanje();
		
		port(8080);
		
		staticFiles.externalLocation(new File("./static").getCanonicalPath());
		
		
		get("/", (req, res) ->{
			
			res.redirect("login.html"); 
			return null;
		});
		
		
		
		post("/rest/login", (req, res) -> {
					
			res.type("application/json");
					
			String data = req.body();
			Korisnik k = g.fromJson(data, Korisnik.class);
					
			
			
			Session ss = req.session(true);
					
			Korisnik korisnik = ss.attribute("korisnik");
					
			for(Korisnik kk: Database.korisnici) {
				if(kk.getEmail().equals(k.getEmail()) && kk.getLozinka().equals(k.getLozinka())  ) {
					if(korisnik == null) {
						korisnik = kk;
						ss.attribute("korisnik", korisnik);	
					}
					return korisnik.getUloga();
				}						
			}
			return false;			
		});
		
		post("/rest/dodajOrganizaciju", (req, res)->{
			String logo = "";
			String oldName = null;
			String name = "";
			String desc = "";
			req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("static/source/logos/"));
			Part uploadFile = req.raw().getPart("file");
			
			Map<String, String[]> paremetri = req.raw().getParameterMap();
			
			name = Arrays.toString(paremetri.get("name")).replace("[", "").replace("]", "");
			desc = Arrays.toString(paremetri.get("desc")).replace("[", "").replace("]", "");
			oldName = Arrays.toString(paremetri.get("oldName")).replace("[", "").replace("]", "");
			System.out.println(oldName);
			if (!oldName.equals("null")) {
				//izmena
				for (Organizacija org : Database.organizacije) {
					if(org.getIme().equals(oldName)) {
						
						org.setIme(name);
						org.setOpis(desc);
						org.setLogo(Sistem.kopirajLogo(uploadFile, org.getLogo()));
						
				
						break;
					}
				}
				
				for (Korisnik k : Database.korisnici) {
					try {
						if(k.getOrganizacijaIme().equals(oldName)) {
							k.setOrganizacijaIme(name);
							break;
						}
					} catch (NullPointerException e) {
						//SUPERADMIN nema organizaciju
					}
					
				}
				
				for (VM vm : Database.VMs) {
					if(vm.getOrganizacijaIme().equals(oldName)) {
						vm.setOrganizacijaIme(name);
						for (Disk d : vm.getDiskovi()) {
							d.setOrganizacijaIme(name);
						}
						break;
					}
					
					
				}
				
				for (Disk d : Database.diskovi) {
					if(d.getOrganizacijaIme().equals(oldName)) {
						d.setOrganizacijaIme(name);
						break;
					}
					
					
				}
				
			}else {
				//dodavanje
				System.out.println("dodata");
				logo = Sistem.kopirajLogo(uploadFile, null);
				Database.organizacije.add(new Organizacija(name, desc, logo, new ArrayList<Korisnik>(),  new ArrayList<String>()));	
			}
			
			Database.saveOrganizacije();
			Database.saveKorisnici();
			Database.saveVMs();
			Database.saveDiskovi();
			
			
			/*
			keys.stream().forEach(key ->{
				String value = String.join(",", paremetri.get(key));
				System.out.println(key +": "+value );
			});
			*/
			
			
			return 1;
		});
		
		post("/rest/popuniAdmin", (req, res) ->{
			Session ss = req.session();
			
			Korisnik k = ss.attribute("korisnik");
			
			if (k == null || k.getUloga()!=Uloga.ADMIN) {
				res.status(403);
				return "";
			}
			
			Organizacija org = Database.getOrganzacijaByName(k.getOrganizacijaIme());
			
			System.out.println(org.toString());
			
			return g.toJson(org);
		});
		
		
		post("/rest/dobaviVM", (req, res)->{
			res.type("application/json");
			
			Korisnik logged = req.session().attribute("korisnik");
			
			String payload = req.body();
			
			JsonParser jp = new JsonParser();
			
			JsonObject job = jp.parse(payload).getAsJsonObject();
			
			String vmIme = job.get("ime").getAsString();
			String vmOrg = job.get("org").getAsString();
			
			if(logged.getUloga()==Uloga.ADMIN) {
				vmOrg = logged.getOrganizacijaIme();
			}
			
			job = new JsonObject();
			job.add("vm", jp.parse(g.toJson(Database.getVMByName(vmIme))));
			
			job.add("svi", jp.parse(g.toJson(Database.getAllAvailableDiskoviByOrganizacijaName(vmOrg))));
			
			
			job.add("sveKat", jp.parse(g.toJson(Database.kategorije)));
			
	
			return g.toJson(job);
		});
		
		
		post("/rest/check", (req, res) ->{
			
			Session ss = req.session();
			Korisnik kk = ss.attribute("korisnik");
			
			if(kk == null || kk.getUloga() != Uloga.KORISNIK ) {
				res.status(403);
			}
			return "";
			
		});
		
		post("/rest/popuniVMs", (req, res)->{
			
			Session ss = req.session();
			Korisnik kk = ss.attribute("korisnik");
			
			if (kk!=null) {
				if(kk.getUloga()==Uloga.ADMIN || kk.getUloga()==Uloga.KORISNIK) {
					return g.toJson(Database.getVMsByOrganizacijaName(kk.getOrganizacijaIme()));
				}else {
					return g.toJson(Database.VMs);
				}
			}
			
			return "";
		
			
		});
		
		post("/rest/popuniKorisnike", (req, res)->{
			
			
			JsonArray jar = new JsonArray();
			
			Session ss = req.session();
			Korisnik kk = ss.attribute("korisnik");
			
			if(kk.getUloga()==Uloga.SUPER_ADMIN) {
				
				for (Korisnik k : Database.korisnici) {
					if(k.getUloga()!=Uloga.SUPER_ADMIN) {
						JsonObject job = new JsonObject();
						job.addProperty("email", k.getEmail());
						job.addProperty("ime", k.getIme());
						job.addProperty("prezime", k.getPrezime());
						job.addProperty("organizacijaIme", k.getOrganizacijaIme());
						
						jar.add(job);
					}
				}
			}else if(kk.getUloga() == Uloga.ADMIN) {
				for (Korisnik k : Database.korisnici) {
					if(k.getUloga()!=Uloga.SUPER_ADMIN && k.getOrganizacijaIme().equals(kk.getOrganizacijaIme())) {
						JsonObject job = new JsonObject();
						job.addProperty("email", k.getEmail());
						job.addProperty("ime", k.getIme());
						job.addProperty("prezime", k.getPrezime());
				
						jar.add(job);
					}
				}
				
			}
			
			
			
			return g.toJson(jar);
		});
		
		
		
		post("/rest/izmeniVM", (req, res) ->{
			res.type("application/json");
			String payload = req.body();
			
			Korisnik k = req.session().attribute("korisnik");
			
			JsonObject job = new JsonParser().parse(payload).getAsJsonObject();
			
			if(k.getUloga()==Uloga.ADMIN) {
				job.addProperty("org", k.getOrganizacijaIme());
			}
			
			Database.updateVM(job);
			
			return true;
			
		});
		
		post("/rest/izmeniKorisnika", (req, res) ->{
			res.type("application/json");
			String payload = req.body();
			Korisnik k = g.fromJson(payload, Korisnik.class);
			
			Korisnik old = req.session().attribute("korisnik");
			
			JsonObject job = new JsonParser().parse(payload).getAsJsonObject();
			String oldMail;
			try {
				 oldMail= job.get("oldEmail").getAsString();
			} catch (NullPointerException e) {
				oldMail=null;
			}
			
			
			boolean done;
			if(oldMail !=null) {
				 Korisnik newK = Database.updateProfile(k, old);
				 req.session().attribute("korinik", newK);
				 return true;
			}else {
				done = Database.updateKorisnik(k);
				return done;
			}

			
			
		});
		
		
		
		get("/rest/dobaviProfil", (req, res) ->{
			res.type("application/json");
			Korisnik k = req.session().attribute("korisnik");
			return g.toJson(k);
		});
		
		post("/rest/obrisiKorisnika", (req, res) ->{
			res.type("application/json");
			
			Korisnik logged = req.session().attribute("korisnik");
	
			String payload = req.body();
			Korisnik k = g.fromJson(payload, Korisnik.class);
			
			if(logged.getEmail().equals(k.getEmail())) {
				res.status(400);
				return "Ne mo�ete obrisati sebe";	
			}
			
			boolean done = Database.deleteKorisnikByEmail(k.getEmail());
			return done;
		});
		
		post("/rest/dodajKorisnikaSA", (req, res) ->{
			res.type("application/json");
			String payload = req.body();
			
			Session ss = req.session();
			
			Korisnik logged = ss.attribute("korisnik");
			
			Korisnik k = g.fromJson(payload, Korisnik.class);
			
			if(logged.getUloga()==Uloga.ADMIN) {
				k.setOrganizacijaIme(logged.getOrganizacijaIme());
			}
		
			boolean done = Database.addKorisnik(k);
			return done;
		});

		post("/rest/popuni", (req, res) ->{
			
			Korisnik k = req.session().attribute("korisnik");
			
			if(k==null || k.getUloga() != Uloga.SUPER_ADMIN) {
				res.status(403);
				return "";
			}
			
			System.out.println(k.getUloga());
			
			JsonArray jar = new JsonArray();
			
			

			for (Organizacija org : Database.organizacije) {
				JsonObject job = new JsonObject();
				job.addProperty("ime", org.getIme());
				job.addProperty("opis", org.getOpis());
				job.addProperty("logo", org.getLogo());
				
				jar.add(job);
			}
			System.out.println(jar.toString());
			
			return g.toJson(jar);
		});

		post("/rest/provOrgIme", (req, res)->{
			
			return Sistem.imePostoji(req.body());
		});
		
		post("/rest/provEmail", (req, res)->{
			
			return Sistem.emailPostoji(req.body());
		});
		
		post("/rest/provVIme", (req, res) ->{
			
			return Sistem.VImePostoji(req.body());
		});
		
		post("/rest/provDiskIme", (req, res) ->{
			
			return Sistem.DiskImePostoji(req.body());
		});
		
		post("/rest/provKatIme", (req, res) ->{
			
			return Sistem.KatImePostoji(req.body());
		});
		get("/rest/odjava", (req, res) ->{
			
			Session ss = req.session(true);
			Korisnik user = ss.attribute("korisnik");
			
			if (user != null) {
				ss.invalidate();
			}
			return true;
		});
		
		post("/rest/upaliVM", (req, res) ->{
			res.type("application/json");
			
			JsonObject job = new JsonParser().parse(req.body()).getAsJsonObject();
			
			String name = job.get("name").getAsString();
			
			return g.toJson(Database.turnOn(name));
			
			
		});
		
		post("/rest/ugasiVM", (req, res) ->{
			res.type("application/json");
			
			JsonObject job = new JsonParser().parse(req.body()).getAsJsonObject();
			String name = job.get("name").getAsString();
			
			return g.toJson(Database.turnOff(name));
			
			
		});
		
		
		///mojsSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS
		
		get("/rest/dobaviOrganizacije", (req, res)->{
			res.type("application/json");
			
			return g.toJson(Database.getAllOrganizacijeNames());
		});
		
		post("/rest/dobaviKategorije", (req, res)->{
			res.type("application/json");
		
			return g.toJson(Database.getAllKategorijeFormated());
		});
		
		
		post("/rest/dobaviDiskove", (req, res)->{
			res.type("application/json");
			
			Korisnik logged = req.session().attribute("korisnik");
			
			JsonObject job = new JsonParser().parse(req.body()).getAsJsonObject();
			
			String org = job.get("org").getAsString();
			
			if(logged.getUloga()==Uloga.ADMIN) {
				org = logged.getOrganizacijaIme();
			}
			
			
			return g.toJson(Database.getAllAvailableDiskoviByOrganizacijaName(org));
		});
		
		post("/rest/dodajVM", (req, res) ->{
			res.type("application/json");
			String payload = req.body();
			Korisnik k = req.session().attribute("korisnik");
			JsonObject job = new JsonParser().parse(payload).getAsJsonObject();
			
			if(k.getUloga()==Uloga.ADMIN) {
				job.addProperty("org", k.getOrganizacijaIme());
			}
			
			Database.addVM(job);
			
			return true;
			
		});
		
		
		
		post("/rest/obrisiVM", (req, res) ->{
			res.type("application/json");
			String payload = req.body();
			JsonObject job = new JsonParser().parse(payload).getAsJsonObject();
	
			String vmName = job.get("name").getAsString();
			
			boolean done = Database.deleteVMByName(vmName);
			
			return done;
			
		});
		
		
		
		post("/rest/popuniDiskove", (req, res) ->{
			res.type("application/json");
			
			Korisnik logged = req.session().attribute("korisnik");
			
			if(logged.getUloga()==Uloga.ADMIN || logged.getUloga()==Uloga.KORISNIK) {
				ArrayList<Disk> diskovi = new ArrayList<Disk>(Database.diskovi);
				diskovi.removeIf(d -> !d.getOrganizacijaIme().equals(logged.getOrganizacijaIme()));
				return g.toJson(diskovi);	
			}
			
			return g.toJson(Database.diskovi);	
		});
		
		
		
		post("/rest/dobaviDisk", (req, res) ->{
			res.type("application/json");
			
			JsonObject jobToReturn = new JsonObject();
			JsonParser jp = new JsonParser();
			
			JsonObject job = jp.parse(req.body()).getAsJsonObject();
			String diskName = job.get("ime").getAsString();
			
			Disk d = Database.getDiskByName(diskName);
			
			jobToReturn.add("disk", jp.parse(g.toJson(d)));
			
			ArrayList<String> vms = Database.getAllVMsNamesByOrgName(d.getOrganizacijaIme());
			vms.removeIf(vm -> vm.equals(d.getVirtuelnaMasinaIme()));
			
			jobToReturn.add("vms", jp.parse(g.toJson(vms)));
			
			return g.toJson(jobToReturn);
		});
		
		post("/rest/izmeniDisk", (req, res) ->{
			res.type("application/json");
			
			JsonObject job = new JsonParser().parse(req.body()).getAsJsonObject();
			
			
			boolean done = Database.updateDisk(job);
			
			return done;
		});
		
		post("/rest/dobaviVMs", (req, res)->{
			res.type("application/json");
			
			Korisnik logged = req.session().attribute("korisnik");
			
			JsonObject job = new JsonParser().parse(req.body()).getAsJsonObject();
			
			String org = job.get("org").getAsString();
			
			if(logged.getUloga()==Uloga.ADMIN) {
				org = logged.getOrganizacijaIme();
			}
			
				
			return g.toJson(Database.getAllVMsNamesByOrgName(org));
		});
		
		
		
		post("/rest/dodajDisk", (req, res)->{
			res.type("application/json");
			
			Korisnik logged = req.session().attribute("korisnik");
			
			JsonObject job = new JsonParser().parse(req.body()).getAsJsonObject();
			
			
			if(logged.getUloga()==Uloga.ADMIN) {
				job.addProperty("org", logged.getOrganizacijaIme());
				
			}
			
			return Database.addDisk(job);
		});
		
		
		post("/rest/obrisiDisk", (req, res) ->{
			res.type("application/json");
			
			String payload = req.body();
			JsonObject job = new JsonParser().parse(payload).getAsJsonObject();
	
			String dName = job.get("name").getAsString();
			
			boolean done = Database.deleteDiskByName(dName);
			
			return done;
			
		});
		/////////////////////////////////////////////////KATEGorije
		post("/rest/popuniKategorije", (req, res) ->{
			res.type("application/json");
			
			Korisnik logged = req.session().attribute("korisnik");
			
			//if(logged.getUloga()==Uloga.ADMIN || logged.getUloga()==Uloga.KORISNIK) {
				//ArrayList<Disk> diskovi = new ArrayList<Disk>(Database.diskovi);
				//diskovi.removeIf(d -> !d.getOrganizacijaIme().equals(logged.getOrganizacijaIme()));
				//return g.toJson(diskovi);	
			//}
			
			
			
			
			return g.toJson(Database.kategorije);	
		});
		
		post("/rest/dodajKategoriju", (req, res)->{
			res.type("application/json");
			
			Korisnik logged = req.session().attribute("korisnik");
			
			Kategorija k = g.fromJson(req.body(), Kategorija.class);
			
			
			
			return Database.addKatetorija(k);
		});
		
		post("/rest/izmeniKategoriju", (req, res)->{
			res.type("application/json");
			
			//Korisnik logged = req.session().attribute("korisnik");
			
			JsonObject job = new JsonParser().parse(req.body()).getAsJsonObject();
			
			
			Kategorija k = g.fromJson(job.get("kateg"), Kategorija.class);
			
			String old_name = job.get("name").getAsString();
		
			
			return Database.updateKategorija(old_name, k);
		});
		
		
		post("/rest/obrisiKategoriju", (req, res)->{
			res.type("application/json");
			
			//Korisnik logged = req.session().attribute("korisnik");
			
			JsonObject job = new JsonParser().parse(req.body()).getAsJsonObject();
		
			String old_name = job.get("name").getAsString();
		
			
			return Database.deleteKategorijaByName(old_name);
		});
		
		
		post("/rest/dobaviIzvestaj", (req, res) ->{
			res.type("application/json");
			
			JsonObject job = new JsonParser().parse(req.body()).getAsJsonObject();
			
			String d1 = job.get("d1").getAsString();
			String d2 = job.get("d2").getAsString();
			
			Korisnik k = req.session().attribute("korisnik");
			
			return g.toJson(Database.getReport(d1, d2, k.getOrganizacijaIme()));
		});
		
		
	}

}
