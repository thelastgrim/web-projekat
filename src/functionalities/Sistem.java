package functionalities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.Part;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import beans.Aktivnost;
import beans.Disk;
import beans.Kategorija;
import beans.Korisnik;
import beans.Organizacija;
import beans.TipDiska;
import beans.Uloga;
import beans.VM;
import database.Database;
import spark.utils.IOUtils;

public class Sistem {
	
	private static final String DEFAULT_LOGO = "./source/cloud-512.png";
	private static final String LOGO_LOCATION = "./source/logos/";
	
	private static Gson g = new Gson();
	private static FileWriter file;

	public static String kopirajLogo(Part uploadFile, String oldLogo) {
		String logo = "";
		
		if (uploadFile.getSubmittedFileName()!=null) {
			
			try (InputStream inputStream = uploadFile.getInputStream()) {
                OutputStream outputStream = new FileOutputStream("static/source/logos/" + uploadFile.getSubmittedFileName());
                IOUtils.copy(inputStream, outputStream);
                outputStream.close();
            } catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			logo = LOGO_LOCATION + uploadFile.getSubmittedFileName();
			
		}else {
			if(oldLogo==null) {
				logo = DEFAULT_LOGO;
			}else {
				logo = oldLogo;
			}
			
		}
		
		return logo;
	}
	
	public static void punjenjeDiskova() {

		ArrayList<Disk> diskovi = new ArrayList<Disk>();

		//"PRVA" organizacija
			//prva VM
		diskovi.add(new Disk("D240GB", "Prva", "SnailX", TipDiska.HDD, 240));
		diskovi.add(new Disk("MD500-XLegend", "Prva", "SnailX", TipDiska.SSD, 500));
		diskovi.add(new Disk("X2-TeraMonster", "Prva", "SnailX", TipDiska.HDD, 1000));
			//druga VM
		diskovi.add(new Disk("MSax16-Titan", "Prva", "SnailXX", TipDiska.HDD, 2000));
		diskovi.add(new Disk("B6T-Suiton", "Prva", "SnailXX", TipDiska.SSD, 1000));
		//slobodni diskovi
		diskovi.add(new Disk("X2Z-MonsterPower", "Prva", "", TipDiska.SSD, 1500));
		diskovi.add(new Disk("B055-Rampage", "Prva", "", TipDiska.HDD, 3000));
		
		//"DRUGA" organizacija
			//slobodni diskovi
		diskovi.add(new Disk("20B25ZTinyTim", "Druga", "", TipDiska.SSD, 250));
		
		
		String json = new Gson().toJson(diskovi);
		
		try {
			file = new FileWriter(new File("src/database/diskovi.json"));
			file.write(json);
			file.close();
	
		} catch (Exception e) {
			// TODO: handle exception
		}			
	}
	
	
	public static void punjenjeKorisnika() {

		ArrayList<Korisnik> korisnici = new ArrayList<Korisnik>();

		korisnici.add(new Korisnik("sadmin", "sadmin", "Pera", "Peric", null, Uloga.SUPER_ADMIN));
		korisnici.add(new Korisnik("admin", "admin", "Nikola", "Nikolic", "Prva", Uloga.ADMIN));
		korisnici.add(new Korisnik("user", "user", "Gvozden", "Nikolic", "Prva", Uloga.KORISNIK));
		korisnici.add(new Korisnik("user2", "user2", "Zivko", "Nikolic", "Druga", Uloga.KORISNIK));
		
		
		String json = new Gson().toJson(korisnici);
		
		try {
			file = new FileWriter(new File("src/database/korisnici.json"));
			file.write(json);
			file.close();
	
		} catch (Exception e) {
			// TODO: handle exception
		}			
	}
	
	public static void punjenjeKategorija() {

		ArrayList<Kategorija> kategorije = new ArrayList<Kategorija>();

		kategorije.add(new Kategorija("Slow", 2, 1, 2));
		kategorije.add(new Kategorija("Medium", 4, 2, 4));
		kategorije.add(new Kategorija("Fast", 12, 16, 6));
		
		
		
		String json = new Gson().toJson(kategorije);
		
		try {
			file = new FileWriter(new File("src/database/kategorije.json"));
			file.write(json);
			file.close();
	
		} catch (Exception e) {
			// TODO: handle exception
		}			
	}
	
	public static void punjenjeVMs() throws ParseException {

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		ArrayList<VM> vms = new ArrayList<VM>();
		ArrayList<Disk> diskovi = new ArrayList<Disk>();
		ArrayList<Aktivnost> aktivnosti = new ArrayList<Aktivnost>();
		
		Date start = sdf.parse("2/2/2002 12:40");
		Date end = sdf.parse("7/6/2002 15:40");
		aktivnosti.add(new Aktivnost(start, end));
		
		start = sdf.parse("11/8/2006 07:00");
		end = sdf.parse("1/9/2006 14:58");
		aktivnosti.add(new Aktivnost(start, end));
		
		start = sdf.parse("1/9/2009 21:16");
		end = sdf.parse("19/12/2009 05:33	");
		aktivnosti.add(new Aktivnost(start, end));
		
		
		start = sdf.parse("12/7/2010 07:23");
		end = sdf.parse("16/9/2010 14:16");
		aktivnosti.add(new Aktivnost(start, end));
		
		start = sdf.parse("01/6/2020 08:44");
		aktivnosti.add(new Aktivnost(start, null));
		
		diskovi.add(new Disk("D240GB", "Prva", "SnailX", TipDiska.HDD, 240));
		diskovi.add(new Disk("MD500-XLegend", "Prva", "SnailX", TipDiska.SSD, 500));
		diskovi.add(new Disk("X2-TeraMonster", "Prva", "SnailX", TipDiska.HDD, 1000));
		
		Kategorija k = new Kategorija("Slow", 2, 1, 2);
		vms.add(new VM("SnailX", "Prva", k, diskovi, aktivnosti));
		
		aktivnosti = new ArrayList<Aktivnost>();
		
		start = sdf.parse("05/2/2020 09:16");
		aktivnosti.add(new Aktivnost(start, null));
		
		diskovi = new ArrayList<Disk>();
		diskovi.add(new Disk("MSax16-Titan", "Druga", "SnailXX", TipDiska.HDD, 2000));
		diskovi.add(new Disk("B6T-Suiton", "Druga", "SnailXX", TipDiska.SSD, 1000));
		
		k = new Kategorija("Medium", 4, 2, 4);
		vms.add(new VM("SnailXX", "Prva", k, diskovi, aktivnosti));
		aktivnosti = new ArrayList<Aktivnost>();
		start = sdf.parse("12/7/2019 02:30");
		end = sdf.parse("16/9/2019 20:20");
		aktivnosti.add(new Aktivnost(start, end));
		
		k = new Kategorija("Fast", 12, 16, 8);
		vms.add(new VM("FireRabbit", "Druga", k, new ArrayList<Disk>(), new ArrayList<Aktivnost>()));
		
		
		String json = new Gson().toJson(vms);
		
		try {
			file = new FileWriter(new File("src/database/VMs.json"));
			file.write(json);
			file.close();
	
		} catch (Exception e) {
			// TODO: handle exception
		}			
	}
	
	
	private static void punjenjeOrganizacija() {

		ArrayList<Organizacija> t = new ArrayList<Organizacija>();
		ArrayList<Korisnik> ks = new ArrayList<Korisnik>();
		ArrayList<String> resursi = new ArrayList<String>();
		
		resursi.add("SnailX");
		resursi.add("SnailXX");
		resursi.add("D240GB");
		resursi.add("MD500-XLegend");
		resursi.add("X2-TeraMonster");
		resursi.add("X2Z-MonsterPower");
		resursi.add("B055-Rampage");
		ks.add(new Korisnik("admin", "admin", "Nikola", "Nikolic", "Prva", Uloga.ADMIN));
		ks.add(new Korisnik("user", "user", "Gvozden", "Nikolic", "Prva", Uloga.KORISNIK));
		t.add(new Organizacija("Prva", "prvi prvi prvi", DEFAULT_LOGO, new ArrayList<Korisnik>(ks), new ArrayList<String>(resursi)));
		resursi.clear();
		ks.clear();
		
		resursi.add("FireRabbit");
		resursi.add("MSax16-Titan");
		resursi.add("B6T-Suiton");
		resursi.add("20B25ZTinyTim");
		
		ks.add(new Korisnik("user2", "user2", "Zivko", "Nikolic", "Druga", Uloga.KORISNIK));
		t.add(new Organizacija("Druga", "druga druga druga", DEFAULT_LOGO, new ArrayList<Korisnik>(ks), new ArrayList<String>(resursi)));
		ks.clear();
		resursi.clear();
		t.add(new Organizacija("Treca", "treca treca", DEFAULT_LOGO, ks, resursi));
		
		String json = new Gson().toJson(t);
		
		try {
			file = new FileWriter(new File("src/database/organizacije.json"));
			file.write(json);
			file.close();
	
		} catch (Exception e) {
			// TODO: handle exception
		}
				
	}
	
	public static void ucitavanje() {
		File f = new File("src/database/organizacije.json");
		File f2 = new File("src/database/korisnici.json");
		File f3 = new File("src/database/VMs.json");
		File f4 = new File("src/database/diskovi.json");
		File f5 = new File("src/database/kategorije.json");
		
		if(f.length()==0) {
			System.out.println("[System]: 'organizacije.json' napunjen.");
			punjenjeOrganizacija();
		}
		
		if(f2.length()==0) {
			System.out.println("[System]: 'korisnici.json' napunjen.");
			punjenjeKorisnika();
		}
		
		if(f3.length()==0) {
			System.out.println("[System]: 'VMs.json' napunjen.");
			try {
				punjenjeVMs();
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(f4.length()==0) {
			System.out.println("[System]: 'diskovi.json' napunjen.");
			punjenjeDiskova();
		}
		if(f5.length()==0) {
			System.out.println("[System]: 'kategorije.json' napunjen.");
			punjenjeKategorija();
		}
		
			
		
			try {
				FileReader reader = new FileReader(new File("src/database/organizacije.json"));
				JsonParser jp = new JsonParser();
				JsonElement obj = jp.parse(reader);
				JsonArray j = obj.getAsJsonArray();
				for (JsonElement jsonElement : j) {
					Organizacija o = g.fromJson(jsonElement, Organizacija.class);
					Database.organizacije.add(o);
				}
				System.out.println("[System]: 'Organizacije' ucitane. ["+j.size()+"] elemenata.");
				
				reader = new FileReader(new File("src/database/korisnici.json"));
				j = jp.parse(reader).getAsJsonArray();
				
				for (JsonElement jsonElement : j) {
					Korisnik k = g.fromJson(jsonElement, Korisnik.class);
					Database.korisnici.add(k);
				}
				System.out.println("[System]: 'Korisnici' ucitani. ["+j.size()+"] elemenata.");
				
				reader = new FileReader(new File("src/database/VMs.json"));
				j = jp.parse(reader).getAsJsonArray();
				
				for (JsonElement jsonElement : j) {
					VM vm = g.fromJson(jsonElement, VM.class);
					Database.VMs.add(vm);
				}
				System.out.println("[System]: 'VMs' ucitane. ["+j.size()+"] elemenata.");
				
				reader = new FileReader(new File("src/database/diskovi.json"));
				j = jp.parse(reader).getAsJsonArray();
				
				for (JsonElement jsonElement : j) {
					Disk d = g.fromJson(jsonElement, Disk.class);
					Database.diskovi.add(d);		
				}
				System.out.println("[System]: 'Diskovi' ucitani. ["+j.size()+"] elemenata.");
				
				reader = new FileReader(new File("src/database/kategorije.json"));
				j = jp.parse(reader).getAsJsonArray();
				
				for (JsonElement jsonElement : j) {
					Kategorija k = g.fromJson(jsonElement, Kategorija.class);
					Database.kategorije.add(k);		
				}
				System.out.println("[System]: 'Kategorije' ucitane. ["+j.size()+"] elemenata.");
	
				reader.close();
		
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	}
	
	public static boolean imePostoji(String payload) {
		JsonParser jp = new JsonParser();
		
		String ime = jp.parse(payload).getAsJsonObject().get("name").getAsString();

		boolean flag = false;
		for (Organizacija o : Database.organizacije) {
			if(ime.equals(o.getIme())) {
				flag = true;
				break;
			}
		}
		return flag;
		
	}

	public static Object emailPostoji(String payload) {
		JsonParser jp = new JsonParser();
		
		String email = jp.parse(payload).getAsJsonObject().get("email").getAsString();

		boolean flag = false;
		for (Korisnik k : Database.korisnici) {
			if(email.equals(k.getEmail())) {
				flag = true;
				break;
			}
		}
		return flag;
	}

	public static Object VImePostoji(String payload) {
		JsonParser jp = new JsonParser();
		
		String ime = jp.parse(payload).getAsJsonObject().get("ime").getAsString();

		boolean flag = false;
		for (VM vm : Database.VMs) {
			if(ime.equals(vm.getIme())) {
				flag = true;
				break;
			}
		}
		return flag;
	}

	public static Object DiskImePostoji(String payload) {
		JsonParser jp = new JsonParser();
		
		String ime = jp.parse(payload).getAsJsonObject().get("ime").getAsString();

		boolean flag = false;
		for (Disk d : Database.diskovi) {
			if(ime.equals(d.getIme())) {
				flag = true;
				break;
			}
		}
		return flag;
	}

	public static Object KatImePostoji(String payload) {
		JsonParser jp = new JsonParser();
		
		String ime = jp.parse(payload).getAsJsonObject().get("ime").getAsString();

		boolean flag = false;
		for (Kategorija k : Database.kategorije) {
			if(ime.equals(k.getIme())) {
				flag = true;
				break;
			}
		}
		return flag;
	}
}
