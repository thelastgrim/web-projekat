package functionalities;

import com.google.gson.JsonParser;

import beans.Organizacija;
import database.Database;

public class OrgFun {
		
	private static JsonParser jp;

	public static boolean imePostoji(String payload) {
		jp = new JsonParser();
		//System.out.println(payload);
		String ime = jp.parse(payload).getAsJsonObject().get("name").getAsString();
		//System.out.println(ime);
		boolean flag = false;
		for (Organizacija o : Database.organizacije) {
			if(ime.equals(o.getIme())) {
				flag = true;
			}
		}
		return flag;
		
	}
}
