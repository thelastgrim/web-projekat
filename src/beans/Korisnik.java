package beans;

public class Korisnik {
	private String email;
	private String lozinka;
	private String ime;
	private String prezime;
	private String organizacijaIme;
	private Uloga uloga;
	
	public Korisnik() {
			
	}
	

	
	public Korisnik(String email, String lozinka, String ime, String prezime, String organizacijaIme, Uloga uloga) {
		super();
		this.email = email;
		this.lozinka = lozinka;
		this.ime = ime;
		this.prezime = prezime;
		this.organizacijaIme = organizacijaIme;
		this.uloga = uloga;
	}



	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLozinka() {
		return lozinka;
	}
	public void setLozinka(String lozinka) {
		this.lozinka = lozinka;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}
	public Uloga getUloga() {
		return uloga;
	}
	public void setUloga(Uloga uloga) {
		this.uloga = uloga;
	}



	public String getOrganizacijaIme() {
		return organizacijaIme;
	}



	public void setOrganizacijaIme(String organizacijaIme) {
		this.organizacijaIme = organizacijaIme;
	}



	@Override
	public String toString() {
		return "Korisnik [email=" + email + ", lozinka=" + lozinka + ", ime=" + ime + ", prezime=" + prezime
				+ ", organizacijaIme=" + organizacijaIme + ", uloga=" + uloga + "]";
	}

	
}


