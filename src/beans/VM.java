package beans;

import java.util.ArrayList;

public class VM{
	
	private String ime;
	
	private String organizacijaIme;
	
	private Kategorija kategorija;
	
	private ArrayList<Disk> diskovi;
	
	public VM() {
		super();
	}

	public VM(String ime, String organizacijaIme, Kategorija kategorija, ArrayList<Disk> diskovi,
			ArrayList<Aktivnost> aktivnosti) {
		super();
		this.ime = ime;
		this.organizacijaIme = organizacijaIme;
		this.kategorija = kategorija;
		this.diskovi = diskovi;
		this.aktivnosti = aktivnosti;
	}

	private ArrayList<Aktivnost> aktivnosti;

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public Kategorija getKategorija() {
		return kategorija;
	}

	public void setKategorija(Kategorija kategorija) {
		this.kategorija = kategorija;
	}

	public ArrayList<Disk> getDiskovi() {
		return diskovi;
	}

	public void setDiskovi(ArrayList<Disk> diskovi) {
		this.diskovi = diskovi;
	}

	public ArrayList<Aktivnost> getAktivnosti() {
		return aktivnosti;
	}

	public void setAktivnosti(ArrayList<Aktivnost> aktivnosti) {
		this.aktivnosti = aktivnosti;
	}

	public String getOrganizacijaIme() {
		return organizacijaIme;
	}

	public void setOrganizacijaIme(String organizacijaIme) {
		this.organizacijaIme = organizacijaIme;
	}
	
}
