package beans;

public class Kategorija {
	public String ime;
	public int brojJezgara;
	public int RAM;
	public int GPUJezgra;
	
	public Kategorija(String ime, int brojJezgara, int rAM, int gPUJezgra) {
		super();
		this.ime = ime;
		this.brojJezgara = brojJezgara;
		this.RAM = rAM;
		this.GPUJezgra = gPUJezgra;
	}
	

	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public int getBrojJezgara() {
		return brojJezgara;
	}
	public void setBrojJezgara(int brojJezgara) {
		this.brojJezgara = brojJezgara;
	}
	public int getRAM() {
		return RAM;
	}
	public void setRAM(int rAM) {
		RAM = rAM;
	}
	public int getGPUJezgra() {
		return GPUJezgra;
	}
	public void setGPUJezgra(int gPUJezgra) {
		GPUJezgra = gPUJezgra;
	}


	@Override
	public String toString() {
		return "Kategorija [ime=" + ime + ", brojJezgara=" + brojJezgara + ", RAM=" + RAM + ", GPUJezgra=" + GPUJezgra
				+ "]";
	}
}
