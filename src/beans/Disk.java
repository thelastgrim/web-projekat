package beans;

public class Disk {
	
	private String ime;
	private String organizacijaIme;
	private String virtuelnaMasinaIme;
	private TipDiska tip;
	private int kapacitet;
	
	public Disk() {
		super();
	}
	public Disk(String ime, String organizacijaIme, String virtuelnaMasinaIme, TipDiska tip, int kapacitet) {
		super();
		this.ime = ime;
		this.organizacijaIme = organizacijaIme;
		this.virtuelnaMasinaIme = virtuelnaMasinaIme;
		this.tip = tip;
		this.kapacitet = kapacitet;
	}
	public Disk(Disk diskByName) {
		this.ime = diskByName.ime;
		this.organizacijaIme = diskByName.organizacijaIme;
		this.virtuelnaMasinaIme = diskByName.virtuelnaMasinaIme;
		this.tip = diskByName.tip;
		this.kapacitet = diskByName.kapacitet;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getOrganizacijaIme() {
		return organizacijaIme;
	}
	public void setOrganizacijaIme(String organizacijaIme) {
		this.organizacijaIme = organizacijaIme;
	}
	public String getVirtuelnaMasinaIme() {
		return virtuelnaMasinaIme;
	}
	public void setVirtuelnaMasinaIme(String virtuelnaMasinaIme) {
		this.virtuelnaMasinaIme = virtuelnaMasinaIme;
	}
	public TipDiska getTip() {
		return tip;
	}
	public void setTip(TipDiska tip) {
		this.tip = tip;
	}
	public int getKapacitet() {
		return kapacitet;
	}
	public void setKapacitet(int kapacitet) {
		this.kapacitet = kapacitet;
	}
	
	
}
