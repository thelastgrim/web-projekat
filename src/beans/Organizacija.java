package beans;

import java.util.ArrayList;

public class Organizacija {
	
	private String ime;
	private String opis;
	private String logo;
	private ArrayList<Korisnik> korisnici;
	private ArrayList<String> resursi;
	
	
	public Organizacija() {
		super();
	}
	public Organizacija(String ime, String opis, String logo, ArrayList<Korisnik> korisnici,
			ArrayList<String> resursi) {
		super();
		this.ime = ime;
		this.opis = opis;
		this.logo = logo;
		this.korisnici = korisnici;
		this.resursi = resursi;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}
	public ArrayList<Korisnik> getKorisnici() {
		return korisnici;
	}
	public void setKorisnici(ArrayList<Korisnik> korisnici) {
		this.korisnici = korisnici;
	}
	
	@Override
	public String toString() {
		return "Organizacija [ime=" + ime + ", opis=" + opis + ", logo=" + logo + ", korisnici=" + korisnici
				+ ", resursi=" + resursi + "]";
	}
	public ArrayList<String> getResursi() {
		return resursi;
	}
	public void setResursi(ArrayList<String> resursi) {
		this.resursi = resursi;
	}
	

}
