package beans;

import java.util.Date;

public class Aktivnost {
	public Date pocetakRada;
	public Date krajRada;
	
	public Date getPocetakRada() {
		return pocetakRada;
	}
	public void setPocetakRada(Date pocetakRada) {
		this.pocetakRada = pocetakRada;
	}
	public Aktivnost(Date pocetakRada, Date krajRada) {
		super();
		this.pocetakRada = pocetakRada;
		this.krajRada = krajRada;
	}
	public Date getKrajRada() {
		return krajRada;
	}
	public Aktivnost() {
		super();
	}
	public void setKrajRada(Date krajRada) {
		this.krajRada = krajRada;
	}
}
